from app.models.letter import Letter


def test_send_letter(client):
    response = client.post("/letters")
    assert response.status_code == 200
    data = response.json
    assert set(data.keys()) == {"id", "la_poste_code", "status"}
    assert data["id"] is not None
    assert data["la_poste_code"] is not None
    assert data["status"] is None


def test_get_letter_by_code(client, db):
    # Add a letter to the database for testing
    with client.application.app_context():
        letter = Letter(la_poste_code="ABCDE12345", status="Delivered")
        db.session.add(letter)
        db.session.commit()

    response = client.get("/letter/ABCDE12345")
    assert response.status_code == 200
    assert "Delivered" in response.json["status"]


class MockResponse:
    def __init__(self, json_data, status):
        self.json_data = json_data
        self.status_code = status

    def json(self):
        return self.json_data


def test_get_suivi_letter_by_code(client, monkeypatch, db):
    # Mock the request to La Poste tracking API
    def mock_get_response(*args, **kwargs):
        json_data = {"shipment": {"event": [{"code": "Delivered"}]}}
        return MockResponse(json_data, 200)

    monkeypatch.setattr("requests.get", mock_get_response)

    # Add a letter to the database for testing
    with client.application.app_context():
        letter = Letter(la_poste_code="ABCDE12345", status=None)
        db.session.add(letter)
        db.session.commit()

    response = client.get("/suivi_letter/ABCDE12345")
    assert response.status_code == 200
    assert "Delivered" in response.json["status"]

    # TODO test missing letter


def test_get_letters_with_tracking(client, monkeypatch, db):
    # Mock the request to La Poste tracking API
    async def mock_get_tracking_data(session, tracking_api_url, headers):
        return {"shipment": {"event": [{"code": "Delivered"}]}}

    monkeypatch.setattr("app.views.letter.get_tracking_data", mock_get_tracking_data)

    # Add letters to the database for testing
    with client.application.app_context():
        letters = [
            Letter(la_poste_code="ABCDE12345", status=None),
            Letter(la_poste_code="FGHIJ67890", status=None),
        ]
        db.session.add_all(letters)
        db.session.commit()

    response = client.get("/letters")
    assert response.status_code == 200
    letter_count = db.session.query(Letter).count()
    assert f"{letter_count} letters status updated" in response.json
