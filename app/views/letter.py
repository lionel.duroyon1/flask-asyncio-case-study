import os
import requests
import asyncio
import time
import logging
from app import db
from app.mocks import send_letter_mock
from app.views import api
from app.models.letter import Letter
from flask import jsonify
from aiohttp import ClientSession
from aiohttp.client_exceptions import ServerDisconnectedError, ClientError

logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)

config = {}
config["LAPOSTE_SUIVI_URL"] = os.environ.get("LAPOSTE_SUIVI_URL", "")
config["CASE_STUDY_SANDBOX_KEY"] = os.environ.get("CASE_STUDY_SANDBOX_KEY", "")


def handle_status(tracking_data):
    """Return status from tracking_data."""
    status = None
    if "shipment" in tracking_data:
        shipment = tracking_data["shipment"]
        if "event" in shipment:
            events = shipment["event"]
            if events:
                event = events[-1]
                if "code" in event:
                    status = event["code"]
    return status


@api.route("/letter/<string:la_poste_code>", methods=["GET"])
def get_letter_by_code(la_poste_code):
    """Get letter by code in local database."""
    # Query the database to find the letter with the given la_poste_code
    letter = Letter.query.filter_by(la_poste_code=la_poste_code).first()

    if letter is not None:
        # Convert the letter to a dictionary using the to_dict method
        letter_dict = letter.to_dict()
        return jsonify(letter_dict)
    else:
        # Return a 404 response if the letter with the provided code is not found
        return jsonify({"message": "Letter not found"}), 404


@api.route("/suivi_letter/<string:la_poste_code>", methods=["GET"])
def get_suivi_letter_by_code(la_poste_code):
    """
    Query laposte api to get status of a letter for a given code, then update status in local db.
    """
    # Query the database to find the letter with the given la_poste_code
    letter = Letter.query.filter_by(la_poste_code=la_poste_code).first()

    if letter is not None:
        # Query the La Poste tracking API
        tracking_api_url = f"{config['LAPOSTE_SUIVI_URL']}/idships/{la_poste_code}"

        headers = {
            "X-Okapi-Key": config["CASE_STUDY_SANDBOX_KEY"]
        }  # Replace with your API key
        response = requests.get(tracking_api_url, headers=headers)

        if response.status_code == 200:
            tracking_data = response.json()
            letter.status = handle_status(tracking_data)

            # Update status in local db
            db.session.add(letter)
            db.session.commit()

        return jsonify(letter.to_dict()), 200
    else:
        # Return a 404 response if the letter with the provided code is not found
        return jsonify({"message": "Letter not found"}), 404


async def get_tracking_data(session, tracking_api_url, headers):
    """Query La Poste tracking API in async."""
    async with session.get(tracking_api_url, headers=headers) as response:
        return await response.json()


@api.route("/letters", methods=["GET"])
def get_letters_with_tracking():
    """
    Query status for all letters in db then update status.
    Do optimize request time we use ayncio to make parallel requests.
    We also implemented a retry mecanism in case of network error.
    """
    letters = Letter.query.all()

    async def fetch(session, letter):
        """Fetch status for a letter."""
        tracking_api_url = (
            f"{config['LAPOSTE_SUIVI_URL']}/idships/{letter.la_poste_code}"
        )
        headers = {"X-Okapi-Key": config["CASE_STUDY_SANDBOX_KEY"]}

        max_retries = 3

        for _ in range(max_retries):
            try:
                # logging.info(f"Fetching {letter.la_poste_code}")
                tracking_data = await get_tracking_data(
                    session, tracking_api_url, headers
                )
                if tracking_data is not None:
                    letter.status = handle_status(tracking_data)
                    db.session.add(letter)
                    break
            except (ClientError, ServerDisconnectedError) as e:
                # Handle network-related errors, e.g., connection timeout or server issues
                logging.error(f"Network Error: {e}")
                await asyncio.sleep(0.1)

    async def fetch_all(letters):
        """Fetch all letters status."""
        async with ClientSession() as session:
            tasks = [fetch(session, letter) for letter in letters]
            await asyncio.gather(*tasks)

    start_time = time.time()
    asyncio.run(fetch_all(letters))
    db.session.commit()
    end_time = time.time()

    return jsonify(
        f"{len(letters)} letters status updated in {end_time - start_time} seconds"
    )


@api.route("/letters", methods=["POST"])
def ep_send_letter():
    """
    Send a letter via La Poste (mocked)
    Get a fake La Poste ID with an empty status.
    """
    # Mocking call to La Poste API to send a letter
    letter_data = send_letter_mock()
    letter = Letter()
    letter.la_poste_code = letter_data["id"]
    db.session.add(letter)
    db.session.commit()
    return letter.to_dict(), 200
