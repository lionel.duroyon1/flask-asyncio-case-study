from flask import Blueprint

api = Blueprint("api", __name__, url_prefix=None)


@api.route("/ping", methods=["GET"])
def ep_ping():
    return "pong", 200
