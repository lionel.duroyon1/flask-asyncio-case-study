import random
import string


def generate_random_la_poste_code():
    """
    Returns a random string of 10 characters
    """
    return "".join(random.choices(string.ascii_uppercase + string.digits, k=10))


def send_letter_mock():
    return {
        "id": generate_random_la_poste_code(),
    }
