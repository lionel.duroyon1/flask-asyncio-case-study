import time
from app import create_app, db
from app.models.letter import Letter
from app.mocks import generate_random_la_poste_code

SANDBOX_LETTERS = [
    {"la_poste_code": "LU680211095FR"},
    {"la_poste_code": "LZ712917377US"},
    {"la_poste_code": "8K00009775862"},
    {"la_poste_code": "861011301731382"},
    {"la_poste_code": "CB662173705US"},
]

NUMBER_OF_LETTERS_TO_GENERATE = 1000

app = create_app()
with app.app_context():
    db.drop_all()
    db.create_all()
    db.session.bulk_insert_mappings(Letter, SANDBOX_LETTERS)

    print("generating")
    generating = time.time()
    letter_mappings = [{"la_poste_code": generate_random_la_poste_code()} for _ in range(NUMBER_OF_LETTERS_TO_GENERATE)]
    print("duration", time.time() - generating)

    print("inserting")
    inserting = time.time()
    db.session.bulk_insert_mappings(Letter, letter_mappings)
    db.session.commit()
    print("duration", time.time() - inserting)

    print(db.session.query(Letter).count(), "letters in db")
