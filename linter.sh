DIRS="app instance tests"
black $DIRS
flake8 $DIRS --max-line-length=100
# consider adding pylint
# pylint $DIRS